import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

//stateful widget
class HelloFlutterApp extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _HelloFlutterAppState();
}

String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String thaiGreeting = "สวัสดี ฟลัตเตอร์";
String franceGreeting = "Bonjour Flutter";

class _HelloFlutterAppState extends State<HelloFlutterApp> {
  String  displayText = englishGreeting;
  // String  displayText = englishGreeting;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home : Scaffold(
        appBar: AppBar(
          title: Text("Hello flutter"),
          leading: Icon(Icons.home_filled),
          actions: <Widget>[
            IconButton(
                onPressed: (){
                  setState(() {
                    displayText = displayText == englishGreeting?
                    spanishGreeting:englishGreeting;
                  });
                },
                icon: Icon(Icons.refresh)),
            IconButton(
                onPressed: (){
                  setState(() {
                    displayText = displayText == englishGreeting?
                    thaiGreeting:englishGreeting;
                  });
                },
                icon: Icon(Icons.local_fire_department)),
            IconButton(
                onPressed: (){
                  setState(() {
                    displayText = displayText == englishGreeting?
                    franceGreeting:englishGreeting;
                  });
                },
                icon: Icon(Icons.local_laundry_service_outlined))
          ],
        ),
        body: Center(
          child: Text(
            displayText,
            style: TextStyle(fontSize:24),
          ),
        ),
      ),
    );
  }
}
